Eine Art Gantt-Diagramm
=======================
...oder zumdindest ein Anzeige von Terminen als Balkendiagramm.
Entsatnden im Rahmen eines Semsterprojeketes, der Rest der Webanwendung für die das Diagramm ursprünglich programmiert
wurde ist hier nicht veröffentlicht.

Features
--------
+ Darstellung über HTML5 Canvas
+ Nutzung mehrere gestackter Canvas als Layer
+ Unabhängig von der Datenstruktur der Webanwendung
+ Elemente sind anklickbar, z.B. um Infoboxen zu den Balken anzuzeigen
+ Farbschema der Balken von außen definierbar

![Diagramm](GanttDiagramm.png)

Dateien
-------
### gantt.js
Dient hier nur als Beispiel, beinhaltet die "glue-funktionen" zum Rest der Webanwendung. Hier wird das Farbschema für
die Balkenkategorien definiert und die Termine aus der Datenbank in die interne Datenstruktur des Diagramms geladen.

### GanttDiagram.js
Basisklasse des Gantt Diagramms. Erzeugt ein Gantt Diagramm, organisiert Layer, updates und Einstellungen wie MAßstab,
Zeitraum, etc.

### GanttLayer.js
Die eigentliche Visualisierung, besteht aus mehreren Klassen die die z.B. Hintergrund, Legende, Buttons, Balken, 
etc. darstellen.

### GanttBar.js
Ein GanttBar speichert bzw.repräsentiert die Daten eines Termins für das GanttDiagramm. Außerdem berechnet
er Darstellungsparameter wie Sichtbarkeit, Beginn und Ende.