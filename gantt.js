document.addEventListener("DOMContentLoaded", init);
window.addEventListener("resize", init);

let ganttDiagram;

/**
 * Gantt Diagram instanziieren und konfigurieren
 * @author Jan-Ole Spahn
 */
function init () {
    let gantt_div = document.getElementById("gantt_div");       // Div für das GanttDiagramm aus DOMtree holen
        /* Gant Diagramm mit aktueller breite des parent divs und fixer Höhe instanziieren */
    ganttDiagram = new GanttDiagram (gantt_div, gantt_div.offsetWidth, 300);

    ganttDiagram.setRange(new Date(), 50);          // Beginn Datum auf heute setzen
    ganttDiagram.update();                          // Diagramm schon mal ohne Termine malen

    Database.getAllAppointments(loadAppointments);  // Termine mit callback aus Datenbank holen

    ganttDiagram.setColorCategories({               // Terminkategorien Fraben zuordnen
         "Webchat": "#8EAFE5" ,
         "Einsendeaufgabe": "#FFB75D",
         "Präsenz": "#EDE657" ,
         "Prüfung": "#886598" ,
         "Sonstiges": "#6EE59B" ,
        });
}

/**
 * Callback Funktion für das Laden der Termine aus der Datenbank. Diese Funktion ist auch die Schnittsetlle zwischen
 * der Struktur der Termine des Typs Appointment in der Datenbank und der internen Datenstruktur des Gantt Diagramms
 * @param aAppointments
 */
function loadAppointments (aAppointments) {
    ganttDiagram.setDbLoaded();     // Damit keine "Datenbank fehlt" Message mehr angezeigt wird

    // Über geladene appointments iterieren, für jedes eine GanttBar erzeugen und an das Diagramm übergeben
    for(let i=0; i < aAppointments.length; i++) {
        let a = aAppointments[i];

            /* Hier erfolgt die Zuordnung der appointment Attribute zu den GanttBar Attributen
               über die Parameter der GanttBar Konstruktors.
             */
        let bar = new GanttBar(
            a.beginDate,                                    // date: Anfangsdatum
            a.endDate === undefined ? null : a.endDate,     // endDate: EndDatum
                                                            // title: zusammengesetzt aus Modulbezeichnung und dem
                                                            // Titel aus appointment
            (a.belongsToModule === "global" ? "" : a.belongsToModule + " " )+ (a.title === "" ? a.category : a.title),
                                                            // tip: ein Text für eine Art Tooltip des GanttBar
            "Titel: " + a.title + ", Kommentar: " + a.comment + ", Kategorie: " + a.category + ", Raum: " + a.place,
            a.category,                                     // categorie
            a.belongsToModule,                              // group
            a.isDeadline(),                                 // isDeadline
        );
        ganttDiagram.addBar(bar);
    }
    ganttDiagram.update();
}

