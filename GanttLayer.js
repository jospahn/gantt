/**
 * Superklasse für alle canvas basierenden Layer des Gantt Diagramms. Stellt Basisfunktionen und Attribute zur verfügung
 * @author Jan-Ole Spahn
 */
class GanttLayer {
    /**
     * Alle Layer werden an der oberen linken Ecke des divs platziert, also alle an der gleichen Stelle.
     * Über die parameter xOffset bzw. yOffset lässt sich der in dem Layer dargestellte Inhalt aber an anderer Stelle
     * innerhalb des canvas positionieren.
     * @param width
     * @param height
     * @param xOffset
     * @param yOffset
     */
    constructor (width, height, xOffset, yOffset){
        let canvas = document.createElement("canvas");
        canvas.width = width;
        canvas.height = height;
        canvas.style.position = "absolute";             // Damit die Canvas Elemente als Layer übereinander liegen


        this._canvas = canvas;
        this._context = canvas.getContext("2d");

        this._width = width;
        this._height = height;
        this._xOffset = xOffset;
        this._yOffset = yOffset;
    }

    /**
     * Setzt den Maßstab pixel pro Tag
     * @param scale
     */
    setScale (scale) {
        this._scale = scale;
    }

    /**
     * Hintergrund Farbe des Layers. Wird hier allerdings noch nicht angewendet sondern als Attribut gespeichert.
     * @param color
     */
    setBackgroundColor (color) {
        this._bgColor = color;
    }

    /**
     * Füllt das Canvas des Layers mit einer Hintergrundfarbe. Sollte bei oben liegenden LAyern normalerweise nicht
     * verwendet werden, da dadurch die Transparenz verloren geht.
     * @param color
     */
    fillBackground (color) {
        this._context.fillStyle = color;
        this._context.fillRect(this._xOffset, this._yOffset, this._width-this._xOffset, this._height-this._yOffset);
    }

    /**
     * Inhalt des canvas löschen
     */
    clear () {
        this._context.clearRect(0, 0, this._width, this._height);
    }

    /**
     * Mausposition innerhalb des Canvas bei einem Klickevent ermitteln.
     * Übernommen von https://www.html5canvastutorials.com/advanced/html5-canvas-mouse-coordinates/
     * @param event
     * @returns {{x: number, y: number}}
     */
    getMousePos(event){
        let rect = this._canvas.getBoundingClientRect();
        return {
            x: Math.floor(event.clientX - rect.left),
            y: Math.floor(event.clientY - rect.top)
        }
    }

    /**
     * Gibt das canvas element des Layers zurück, so dass es von einer externen Funktion in den DOMtree eingehängt
     * werden kann
     * @returns {HTMLElement}
     */
    getCanvas() {
        return this._canvas;
    }

    /**
     *  Diese Methode erstellt ein Rechteck mit abgerundeten Ecken.
     *  - Wird nur ein  Radius angegeben, gilt er für alle Ecken.
     *  - Werden zwei Angegebn, gelten diese für Links oben/unten sowie für rechts oben/unten.
     *  - Werden vier radien angegeben, hat jede Ecke ihren eigenen radius.
     *  Wichtig: Diese Methode erstellt nur den Path, im Anschluss an den Methodenaufruf,
     *  muss zusätzlich this._context.fill(); und/oder this._context.stroke(); aufegrufen werden.
     *
     * @param x - Obere linke Ecke
     * @param y - Obere linke Ecke
     * @param w - Gesamtbreite des Rechtecks
     * @param h - Gesamthöhe des Rechtecks
     * @param radL - Radius oben links (oder oben und unten links)
     * @param radR - Optional: Radius Oben Rechts (oder oben und unten rechts)
     * @param radBR - Optional: Radius unten rechts
     * @param radBL - OPtional: Radius unten links
     * @private
     */
    roundedRect (x, y, w, h, radL, radR, radBR, radBL) {
        if ( radR === undefined) {          // Wurde nur ein radius angegebe, für alle Ecken verwenden
            radR = radL;
            radBR = radL;
            radBL =  radL;
        } else if (radBR === undefined) {  // Wurden zwei angegeben, für links bzw. rechts verwenden
            radBR = radR;
            radBL = radL;
        }

        this._context.beginPath();
        this._context.moveTo(x+radL, y);
        this._context.arcTo(x+w, y, x+w, y+radR, radR);
        this._context.arcTo(x+w, y+h, x+w-radBR, y+h, radBR);
        this._context.arcTo(x, y+h, x, y+h-radBL, radBL);
        this._context.arcTo(x, y, x+radL, y, radL);
    }
}

/**
 * Layer für das Tagesraster
 * @author Jan-Ole Spahn
 */
class GanttGridLayer extends GanttLayer{
    /**
     * Konstruktor, Parameter entsprechen denen der Superklasse
     * @param width
     * @param height
     * @param xOffset
     * @param yOffset
     */
    constructor (width, height, xOffset, yOffset) {
        super (width, height, xOffset, yOffset );
    }

    /**
     * Farbe und Dicke der Rasterlinien
     * @param color
     * @param width
     */
    setLineStyle (color, width) {
        this._context.strokeStyle = color;
        this._context.lineWidth = width;
    }

    /**
     * Raster malen
     */
    drawGrid () {
        this.fillBackground(this._bgColor);
        let x = this._xOffset,                      // Position der nächsten Linie (hier also die der ersten)
            days = (this._width - x) / this._scale; // Anzahl der Tage im Diagramm
        for (let i = 0; i<=days; i ++)
        {
            this._context.beginPath();
            this._context.moveTo(x, this._yOffset);
            this._context.lineTo(x, this._height);
            this._context.stroke();
            x += this._scale;
        }
    }
}

/**
 * Layer mit dr Tageslegende
 * @author Jan-Ole Spahn
 */
class GanttDateLegend extends GanttLayer{
    /**
     * Konstruktor, Parameter entsprechen denen der Superklasse
     * @param width
     * @param height
     * @param xOffset
     * @param yOffset
     */
    constructor (width, height, xOffset, yOffset ) {
        super (width, height, xOffset, yOffset);
        this._context.font = "15px OpenSans";
        this._context.textBaseline = "top";
        this._textColor = "white";
    }

    /**
     * Legende malen
     * @param date Startdatum des Diagramms
     */
    drawDateLegend (date) {
        this.clear();
        this.fillBackground(this._bgColor);

        date = new Date(date);  // Datum in neue VAriable kopieren damit Änderunge lokal bleiben


        let x = this._xOffset,
            days = Math.floor((this._width - x) / this._scale); // Anzahl der Tage im Diagramm berechnen

        this._context.fillStyle = this._textColor;

        for (let i = 0; i<days; i ++)
        {
            let text = date.getDate() ;

            this._context.fillText(text, x + 5, this._yOffset + 5 );

            /* Der Monat wir unter der Tagesspalte mit dem 1. des Monats und unter der ersten Tagesspalte im
               Diagramm angezeigt. Letzteres aber nur, wenn der erste Tag vor dem 25. des Monats liegt, da sonst zwei
               Monatsnamen kurz hintereinander stünden.
             */
            if (date.getDate() === 1 || (date.getDate() < 25 && i === 0)) {
                text = Util.getMonthName(date.getMonth());
                this._context.fillText(text, x + 5, this._yOffset + 25 );
            }

            date.setDate(date.getDate() + 1);   // Datum einen tag weiter zählen
            x += this._scale;
        }
    }
}

/**
 * In diesem Layer werdn die Terminbalken dargestellt
 */
class GanttBarLayer extends GanttLayer {
    /**
     * Konstruktor
     * @param width
     * @param height
     * @param xOffset
     * @param yOffset
     * @param barHeight Pixelhöhe der Terminbalken
     * @param barVerticalGap Pixelbastand zwischen den Balken
     */
    constructor (width, height, xOffset, yOffset, barHeight, barVerticalGap) {
        super (width, height, xOffset, yOffset );
        this._barHeight = barHeight;
        this._barVerticalGap = barVerticalGap;

        this._cursor = 0;   // Cursor zum Merken der aktuellen y-Position beim Malen der Balken
    }

    /**
     * Nächsten Balken malen, bei jedem aufruf wird eine Zeile weiter unten gemalt
     * @param parameter x-Position und Länge in Tagen
     * @param color
     * @param deadline
     * @returns {{x: number, y: *, w: number, h: *}} siehe unten
     */
    drawNext (parameter, color, deadline ) {
        /* Pixel aus Tagesposition und Tagesanzahl berechenen */
        let pos = parameter.pos * this._scale;
        pos += this._xOffset;
        let width = parameter.length * this._scale;

        let y = this._barVerticalGap + this._cursor * (this._barHeight + this._barVerticalGap);

        /* Die Balken werden am anfang und Ende abgerundet, außer die zugeghörigen Termine gehen über das Diagramm hinaus */
        let radiusLeft = parameter.before ? 0 : 7;
        let radiusRight = parameter.after ? 0 : 7;

        /* Schatten malen */
        this._context.fillStyle = "grey";
        this.roundedRect(pos+1, y+1, width+1, this._barHeight+1, radiusLeft, radiusRight);
        this._context.fill();

        /* Balken malen */
        this._context.fillStyle = color;
        this.roundedRect(pos, y, width, this._barHeight, radiusLeft, radiusRight );
        this._context.fill();

        /* Deadline punkt malen */
        if (deadline) {
            let diameter = this._barHeight - 4;
            this._context.fillStyle = "rgb(200,29,44)";
            this.roundedRect(pos+width-4-diameter, y+2, diameter, this._barHeight-4, diameter/2);
            this._context.fill();
        }

        this._cursor++;

        /* Zurückgeben von Pixelpositionen und Abmessungen, damit klickbare Flächen und Legenden positioniert werden können */
        return {
            x: pos,                 // x-Position in pixeln
            y: y,                   // y-Position in pixeln
            w: width,
            h: this._barHeight
        };
    }

    /* Alle Balken entfernen */
    reset () {
        this.clear();
        this._cursor = 0;
    }
}

/**
 * Layer für die Beschriftung links neben den Terminbalken
 * @author Jan-Ole Spahn
 */
class GanttLeftLegend extends GanttLayer {
    /**
     * Konstruktor
     * @param width
     * @param height
     * @param xOffset
     * @param yOffset
     * @param indent    Textabstand vom linken Rand
     */
    constructor (width, height, xOffset, yOffset, indent ) {
        super(width, height, xOffset, yOffset);

        this._indent = indent;
        this._textColor = "white";
    }

    /**
     * Alles löschen
     */
    reset () {
        this.clear();
        this.fillBackground(this._bgColor);
        this._context.fillStyle = this._textColor;
    }

    /**
     * Beschriftung für eine Zeile schreiben
     * @param title
     * @param y
     * @param height
     */
    writeTitle(title, y, height) {
        let baseLine = y + height;
        this._context.font = "15px OpenSans";
        this._context.fillText(title, this._indent, baseLine,);
    }
}

/**
 * Layer zum Anzeigen von Hinweisboxen auf dem Diagrammfeld
 * @author Jan-Ole Spahn
 */
class GanttMessageLayer extends GanttLayer {
    /**
     * Konstruktor
     * @param width
     * @param height
     * @param xOffset
     * @param yOffset
     */
    constructor (width, height, xOffset, yOffset ) {
        super(width, height, xOffset, yOffset);
        this._context.textAlign = "center";
        this._context.textBaseline = "middle";
    }

    /**
     * Hinweisbox entfernen
     */
    reset () {
        this.clear();
    }

    /**
     * Hinweisbox darstellen. Diese wird im Digarmmfeld zentriert
     * @param text Anzuzeigender Text
     * @param color Hintergrundfarbe
     * @returns {{x: *, y: *, w: number, h: number}}    Position und Größe des Rechtecks
     */
    message (text, color ) {
        this._context.font = "bold 30px OpenSans";
        /* Ermittlung der Textabmessungen um Position und größe des Rechteckes berechnen zu können */
        let textMeasures = (this._context.measureText(text)),
            boxWidth = Math.ceil(textMeasures.width * 1.3),
            boxHeight = 75,
            areaWidth = this._width - this._xOffset,
            areaHeight = this._height - this._yOffset,
            boxX = areaWidth/2 - boxWidth/2 + this._xOffset,
            boxY = areaHeight/2 - boxHeight/2 + this._yOffset;

        /* Rechteck malen */
        this._context.fillStyle = color;
        this.roundedRect(boxX, boxY, boxWidth, boxHeight, 10);
        this._context.fill();

        /* Text schreiben */
        this._context.fillStyle = "black";
        this._context.fillText(text, areaWidth/2 + this._xOffset, areaHeight/2 + this._yOffset );

        return {
            x: boxX,
            y: boxY,
            w: boxWidth,
            h: boxHeight
        }
    }
}

/**
 * Layer für Klickbarebereiche (keine Grafik in diesem Layer) und Buttons (mit Grafikdarstellung in diesem Layer)
 * @author Jan-Ole Spahn
 */
class GanttControlLayer extends GanttLayer {
    /**
     * Konstruktor, Offset werte sind hier immer auf 0, da Buttons ja im gesamten Diagramm platziert werden können
     * @param width
     * @param height
     */
    constructor (width, height) {
        super(width, height, 0, 0);

        let self = this;
        /* Events bei Mausklick, position ermitteln und verarebiten */
        this._canvas.addEventListener("mousedown", function(evt) {
            let pos = self.getMousePos(evt);
            self.handleClick(pos);
        });

        /* Events bei Mausbewegung um den Mauspointer zu veränder wenn er über einer Klickbaren Fläche ist */
        this._canvas.addEventListener("mousemove", function(evt) {
            let pos = self.getMousePos(evt);
            for (let i=0; i<self._buttons.length; i++) {
                let b = self._buttons[i];
                if ((pos.x > b.x && pos.x < (b.x+b.w)) && (pos.y > b.y && pos.y < (b.y+b.h)) ) {
                    self._canvas.style.cursor = "pointer";
                    break;
                }
                else {
                    self._canvas.style.cursor = "default";
                }
            }
        });

        this._buttons = [];     // Array mit den definierten Buttons / klickbaren Bereichen

        this._context.textAlign = "center";
        this._context.textBaseline = "middle";
    }

    /**
     * Mausklick verarebiten
     * @param pos
     */
    handleClick (pos) {
        for (let i=0; i<this._buttons.length; i++) {
            let b = this._buttons[i];
            // Prüfen ob an der Stelle des Klicks ein Button ist und dessen Funktion aufrufen
            if ((pos.x > b.x && pos.x < (b.x+b.w)) && (pos.y > b.y && pos.y < (b.y+b.h)) ) {
                b.func(pos);
            }
        }
    }

    /**
     * Einen Button bzw. eine Klickbare Fläche hinzufügen. Für klickbare Flächen können color und die folgenden
     * Parameter entfallen.
     * @param x     x-Position im Diagramm
     * @param y     y-Position im Diagramm
     * @param w     Breite
     * @param h     Höhe
     * @param func  Funktionspointer auf die auszuführende Funktion
     * @param color Farbe des Buttons
     * @param text  Buttontext
     * @param textcolor
     * @param persistent Wenn true, wird dieser Button nicht von clearButtons() gelöscht. DIese Buttons müssen vor den
     *                      löschbaren hinzugefügt werden!
     */
    addButton (x, y, w, h, func, color, text, textcolor, persistent) {
        if (persistent) {
            this._buttons.unshift({x: x, y: y, w: w, h: h, func: func, persistent: true });
        } else {
            this._buttons.push({x: x, y: y, w: w, h: h, func: func, persistent: false });
        }

        if (color !== undefined) {
            this._context.fillStyle = color;
            this.roundedRect(x, y, w, h, 5);
            this._context.fill();

            if (text !== undefined)
                this._context.fillStyle = textcolor;
                this._context.fillText(text, x + w / 2, y + h / 2);
        }
    }

    /**
     * Buttons löschen.
     */
    clearButtons () {
        for (let i=this._buttons.length-1; i>=0; i--) {
            if ( this._buttons[i].persistent === true)
                break;
            else
                this._buttons.pop();
        }
    }

}


