/**
 * Basisklasse des Gantt Diagramms. Erzeugt ein Gantt Diagramm, organisiert Layer, updates und Einstellungen wie MAßstab,
 * Zeitraum, etc.
 * @author Jan-Ole Spahn
 */
class GanttDiagram {

    /**
     * Erzeugt ein GanttDiagramm Objekt unter Angabe des parent divs, der Breite (sollte der des parent divs entsprechen)
     * und der gewünschten Höhe (wird dann auch für das parent div übernommen)
     * @param parent Referenz auf das HTML div in dem das Diagramm dargestellt werden soll
     * @param width Hier sollte die aktuelle Breite des divs angegebene werden
     * @param height Hier sollte die gewünschte höhe des Diagramms angegeben werden
     */
    constructor (parent, width, height) {
        parent.style.height = '300px';

        this._parent = parent;
        this._width = width;
        this._dbLoaded = false;     // Wird wahr wenn erfolgreich Daten geladen wurden

        /* diverse Parameter für die Größeneinstellungen im Diagramm */
        this._bar_height = 20;
        this._bar_vertical_gap = 5;
        this._left_legend_width = 150;
        this._date_legend_height = 50;
        this._max_bars = 10;

        /* Erzeugen des Layers mit dem Raster */
        this._gridLayer = new GanttGridLayer(width, height - this._date_legend_height, this._left_legend_width, 0);
        this._gridLayer.setLineStyle("#AAAAAA", 1.1);
        this._gridLayer.setBackgroundColor("WhiteSmoke");

        /* Erzeugen des Layers für die Balken */
        this._barLayer = new GanttBarLayer(width, height, this._left_legend_width, 0, this._bar_height, this._bar_vertical_gap);    //ToDO: Nach unten begrenzen

        /* Erzeugen der Layer für die linke Balkenlegende und die Datumslegende unten */
        this._leftLegendLayer = new GanttLeftLegend(this._left_legend_width, height - this._date_legend_height, 0, 0, 5);
        this._leftLegendLayer.setBackgroundColor("#444444");
        this._dateLegendLayer = new GanttDateLegend(width, height, this._left_legend_width, height-this._date_legend_height);
        this._dateLegendLayer.setBackgroundColor("#444444");

        /* Erzeugen der Layer für Messagebox und Buttons */
        this._messageLayer = new GanttMessageLayer(width, height-this._date_legend_height, this._left_legend_width, 0);
        this._controlLayer = new GanttControlLayer(width, height);
        this._controlLayer.addButton(10, height-40, 60, 30, this.scrollBack.bind(this), "#6EC6E5", "<<", "white", true);
        this._controlLayer.addButton(80, height-40, 60, 30, this.scrollForward.bind(this), "#6EC6E5", ">>", "white", true);

        /* Ein Array für die Termine als GanttBars */
        this._bars = [];

        /* Layer (canvas) in den DOM Tree einhängen. Wichtig: klickbares layer zuletzt, es muss on top liegen */
        this._parent.appendChild(this._gridLayer.getCanvas());
        this._parent.appendChild(this._leftLegendLayer.getCanvas());
        this._parent.appendChild(this._dateLegendLayer.getCanvas());
        this._parent.appendChild(this._barLayer.getCanvas());
        this._parent.appendChild(this._messageLayer.getCanvas());
        this._parent.appendChild(this._controlLayer.getCanvas());
    }

    /**
     * Diagramm zurück scrollen (einen Tag eher beginnen)
     */
    scrollBack () {
        this.scrollDiagram(-1);
    }

    /**
     * Diagramm vor scrollen (einen Tag später beginnen)
     */
    scrollForward () {
        this.scrollDiagram(1);
    }

    /**
     * Diagramm um gegeben Anzahl an tagen vor oder zurück scrollen. Also Start Datum aktualisieren und alle Balken
     * anpassen. Führt danach update() aus.
     * @param days Anzahl der Tag, negativer Wert: früher Beginnen, positiver Wert: später beginnen
     */
    scrollDiagram(days) {
        this._startDate.setDate(this._startDate.getDate() + days);

        for (let i = 0; i<this._bars.length; i++) {
            this._bars[i].scrollDiagram(days);
        }
        this.update();
    }

    /**
     * Legt das Stardatum des Diagramms sowie den Maßstab fest.
     * @param date Beginndatum als js date() Objekt
     * @param scale Integerwert pixel pro Tag
     */
    setRange (date, scale) {
        this._startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        this._dayScale = scale;
        this._days = (this._width - this._left_legend_width) / scale;

        this._gridLayer.setScale(scale);
        this._barLayer.setScale(scale);
        this._dateLegendLayer.setScale(scale);
    }

    /**
     * Zurücksetzen dynamische Layer Inhalte und neu malen.
     */
    update () {
        /* Zurücksetzten der Layer, d.h. löschen der dynamischen Inhalte */
        this._barLayer.reset();
        this._leftLegendLayer.reset();
        this._messageLayer.reset();
        this._controlLayer.clearButtons();

        /* Wenn bsiher noch keine Datenbankgeladen wurde bzw. geladen werden konnte, einen Hinweis im GanttDiagramm anzeigen */
        if (this._dbLoaded === false) {
            let rect = this._messageLayer.message("Hier klicken => zu den Semestereinstellungen", "rgba(255, 0, 0, 0.5)");
            this._controlLayer.addButton(rect.x, rect.y, rect.w, rect.h, function(){
                window.location = "semester-setting.html";
            });
        }
        /* Wenn zwar eine DB geladen wurde, diese aber keine Termine enthält, Message anzeigen */
        else if (this._bars.length === 0) {
            let rect = this._messageLayer.message("Hier klicken => Termine anlegen", "rgba(110, 229, 115, 0.5)");
            this._controlLayer.addButton(rect.x, rect.y, rect.w, rect.h, function(){
                window.location = "calendar.html";
            });
        /* wenn erfolgreich Termine aus der Datenbak geladen wurde ... */
        } else {
            let cntVisible = 0;  // Zählt die sichtbaren Balken
                /* Überlauf der Array Länge und Überlauf des Diagramms nach unten verhinden */
            let max = this._max_bars > this._bars.length ? this._bars.length : this._max_bars;
            for (let i = 0; i<max; i++) {
                let bar = this._bars[i];
                if (bar.isVisible()) {                                  // Wenn der Balken (Termin) im sichtbaren Datumsfenster liegt ...
                    let parameter = bar.getBarPosLength();              // Koordinaten holen
                    let color = this._aColorCategories[bar.category];   // Frabe festlegen
                    let rect = this._barLayer.drawNext(parameter, color, bar.isDeadline());     // Balken malen
                    this._leftLegendLayer.writeTitle(bar.getTitle(), rect.y, 15);               // Legende schreiben
                    this._controlLayer.addButton(rect.x, rect.y, rect.w, rect.h, function(){    // Balken klickbar machen
                        alert(bar._tip);
                    });
                    cntVisible++;
                }
            }
            if (cntVisible === 0)       // Wenn kein Termin im fenster lag, Message anzeigen.
                this._messageLayer.message("Terminfreie Zeit :-)", "rgba(110, 229, 115, 0.5)");
        }

        this._gridLayer.drawGrid();
        this._dateLegendLayer.drawDateLegend(this._startDate);
    }

    /**
     * Dem GanttDiagramm einen Terminbalken hinzufügen. Das stellt den Balken noch nicht dar, sonder fügt ihm nur der
     * interenen Datenhaltung hinzu!
     * @param bar   Ein terminbalken des Typs GanttBar
     */
    addBar (bar) {
        bar.setDiagramRange(this._startDate, this._days);       // Dem Balken das aktuelle Datumsfenster mitgeben
        //if (bar.isVisible())
        //    this._noOfVisibleBars++;
        this._bars.push(bar);                                   // und ins Array pushen
    }

    /**
     * Festlegung der farbzuordnung von Balkenkategorien. Die Kategorienamen sollten natürlich denen in den Terminen
     * aus der DAtenbank enstprechen, sind aber im Prinzip beliebige Strings.
     * @param cc Javascript Objekt mit Zuordnung {"Kategorie": "Farbe"}
     */
    setColorCategories (cc) {
        this._aColorCategories = cc;
    }

    /**
     * Setzt das intern Flag, das eine Datenbank erfolgreich geladen wurde.
     */
    setDbLoaded() {
        this._dbLoaded = true;
    }
}

