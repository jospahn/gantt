/**
 * Ein GanttBar speichert bzw.repräsentiert die Daten eines Termins für das GanttDiagramm. Außerdem berechnet
 * er Darstellungsparameter wie Sichtbarkeit, Beginn und Ende.
 * @author Jan-Ole Spahn
 */
class GanttBar {
    /**
     * Constructor
     * @param date - Das Anfangsdatum bzw. der Zeitpunkt des Ereignisses
     * @param endDate - Das Enddatum eines Zeitraumes, darf auch null sein, dann wird der Balken 1 Tag lang.
     * @param title - Der Titel des Termins (String), zum Anzeigen in der Legenede links neben den Balken
     * @param tip - Weiterer Text zu dem Termin
     * @param category - Z.B. zum Färben von Balken
     * @param group - eventuell später zum Gruppieren von Balken
     */
    constructor (date, endDate, title, tip, category, group, isDeadline) {
        // Abfangen von fehlenden Enddaten bzw. Zeitpunkt Terminen
        if (endDate === undefined || endDate === null || endDate === "")
            endDate = date;

        this._date = date;              // Anfangsadtum
        this._endDate = endDate;        // Enddatum
        this._category = category;      // Kategorie, wird im GanttDiagramm
        this._group = group;            // For future use
        this._title = title;            // Titel für die Legende
        this._tip = tip;                // Wird beim Klick auf den Bar angezeigt
        this._isDeadline = isDeadline;  // Zur hervorhebeung von Deadlines

            /* Dauer des Termins in ganzen Tagen bzw. betroffenen Tagen */
        this._duration = GanttBar.calcDaysTouched(date, endDate);
    }

    /**
     * Setzt den aktuellen Anzeigebereich des Diagramms
     * @param diagramStartDate - Der erste sichtbare Tag
     * @param diagramDays - Die Anzahl sichtbarer Tage
     */
    setDiagramRange (diagramStartDate, diagramDays) {
        this._diagramDays = diagramDays;

        let date = new Date (this._date.getFullYear(), this._date.getMonth(), this._date.getDate());

            /* Berechenen, wie viele ganze Tage vor (beginIdx<0) oder nach (>=0) Diagramm Start der Balken beginnt */
        this._beginIdx = Math.floor(GanttBar.calcHours(diagramStartDate, date) / 24);

            /* Fesstellen, on der Balken überhaupt im angezeihgten Datumsbereich liegt */
        this._calcVisibility();
    }

    /**
     * Verschiebt den Anzeigebereich des Diagramms um Tage nach links oder nach rechts
     * @param days - Anzahl der Tage, negative Anzahl: schieben nach links (eher)
     */
    scrollDiagram (days) {
        this._beginIdx -= days;
        this._calcVisibility();
    }

    isVisible () {
        return this._visible;
    }

    isDeadline () {
        return this._isDeadline;
    }

    /**
     * Gibt die Position und und Länge des Balkens im sichtbaren Diagramm Bereich zurück
     * @returns {*} pos: Tage nach Diagrammbeginn, length: Länge in Tagen, before: Tagesanzahl die der Balken vor
     * dem Diagramm beginnt, after: Tage esanzahl die der Balken nach dem Diagramm endet.
     */
    getBarPosLength () {
        if (this._visible) {
            let pos = this._beginIdx,
                length = this._duration,
                before = 0,
                after = 0;

            if (pos < 0) {
                length += pos; // Negative x werte von width abziehen
                before = -pos;
                pos = 0;      // Negative x werte zu null
            }

            if ((pos + length) > this._diagramDays) {
                after = length;
                length = this._diagramDays - pos;
                after -= length;
            }

            return {
                pos: pos,
                length: length,
                before: before,
                after: after
            }
        }

        return {
            pos: 0,
            length: 0,
            before: 0,
            after: 0
        }
    }

    getTitle () {
        if (typeof this._title === "string")
            return this._title;
        else
            return "";
    }

    getTip () {
        return this._tip;
    }

    get category () {
        return this._category;
    }

    /**
     * Ermittelt ob der Balken oder ein teil davon im aktuellen Diagrammbereich sichtbar ist.
     */
    _calcVisibility () {
        /* Wenn der Balken vor dem Diagrammstart endet oder nach dem Diagrammende beginnt, dann ist er nicht sichtbar */
        if ((this._beginIdx + this._duration <= 0) || (this._beginIdx > this._diagramDays)) {
            this._visible = false;
        } else {
            this._visible = true;
        }
    }


    /**
     * Berechen wie viele Tage von dem termin betroffen sind. Wenn ein Termin z.B. um 23:55 beginnt und am nächsten Tag
     * um 0:05 endet, sind zwei Tage betroffen.
     * @param start
     * @param end
     * @returns {number}
     */
    static calcDaysTouched (start, end) {
        start = new Date(start.getFullYear(), start.getMonth(), start.getDate());
        end = new Date(end.getFullYear(), end.getMonth(), end.getDate(), 1, 0);

        let hours = (GanttBar.calcHours(start, end));       // Dauer in Stunden
        let days = Math.ceil(hours / 24);                   // Dauer in ganzen Tagen

        return days;
    }

    /**
     * Anzahl von Stunden zwischen zwei Daten berechnen (auch über Tagesgrenzen hinweg).
     * Die Berechnung von Stunden anstatt gleich von Tagen ist allegemeiner, außerdem kann dann beim Aufrufer
     * die Auf- oder Abrundung auf ganze Tage erfolgen, je nach Bedarf
     * @param start
     * @param end
     * @returns {number} Anzahl der Stunden zwischen den beiden Daten
     */
    static calcHours (start, end) {
        // ToDo: Eventuell Fehler wegen Zeitumstellungstagen
        if (start == null || end == null)   // NullPointer abfangen
            return 0;
        let millis = end - start;           // Millisekunden zwischen den Daten
        return millis / 3.6e6;              // Millisekunden / ( 60s * 60m * 1000)
    }

}
